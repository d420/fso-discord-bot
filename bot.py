import logging
import asyncio
from environs import Env

import discord
from discord import Game
from discord.message import Message

from simutils import sim_time

logging.basicConfig(level=logging.INFO)

CHANNEL_ID = '560144882077794354'
NOTIFY_MINS = 10

ROLES = {
        'factory': ['560423071073435648'],
        'nightclub': ['560423934257004567'],
        'diner': ['560422768919838732']
}


class SimClient(discord.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.time_track = {
        }

        self.notify_list = {
            'factory': set(),
            'nightclub': set(),
            'diner': set()
        }

        # create the background task and run it in the background
        def reset_task(fut):  # pylint: disable=unused-argument
            task = self.loop.create_task(self.my_background_task())
            task.add_done_callback(reset_task)
        task = self.bg_task = self.loop.create_task(self.my_background_task())
        task.add_done_callback(reset_task)

    async def on_ready(self):
        print(f'Logged in as {self.user.name}')
        print(self.user.name)
        print(self.user.id)
        print('------')

    @staticmethod
    def notifiable(time_left):
        h, m = time_left
        return (h*60 + m) <= NOTIFY_MINS

    async def notify_job(self, job_name, time_left):
        print(job_name, time_left)
        if job_name not in self.time_track:
            self.time_track[job_name] = time_left
        old_time = self.time_track[job_name]
        notifiable = (
            self.notifiable(time_left)
            and not self.notifiable(old_time)
        )
        if not notifiable:
            self.time_track[job_name] = time_left
            return

        _, m = time_left
        user_ids = self.notify_list[job_name]
        self.notify_list[job_name] = set()
        role_ids = ROLES[job_name]
        roles = ' '.join(f"<@&{r}>" for r in role_ids)
        users = ' '.join(f"<@{u}>" for u in user_ids)
        await self.wait_until_ready()
        channel = self.get_channel(CHANNEL_ID)
        await self.send_message(
            channel,
            "{r} {u} {job} shift starts in {m} minutes".format(
                r=roles,
                u=users,
                m=m,
                job=job_name.capitalize())
        )
        self.time_track[job_name] = time_left

    async def playing_time(self, h, m, suff):
        await self.change_presence(game=Game(name=f"{h}:{m:02} {suff}"))

    async def my_background_task(self):
        await self.wait_until_ready()
        while not self.is_closed:
            h, m, suff, times = sim_time()
            await self.playing_time(h, m, suff)
            for job_name, time_left in times.items():
                await self.notify_job(job_name, time_left)
            await asyncio.sleep(5)


def main():
    env = Env()
    env.read_env()
    token = env.str('TOKEN', None)
    user = env.str('USER', None)
    password = env.str('PASSWORD', None)
    client = SimClient()

    @client.event
    async def on_message(message: Message):
        text = message.content.split(maxsplit=1)
        if len(text) != 2:
            return
        if text[0] != '!notify':
            return

        job = text[1]
        if job not in client.notify_list:
            return

        client.notify_list[job].add(message.author.id)
        await client.add_reaction(message, "👌")

    if token:
        client.run(token)
    elif user and password:
        client.run(user, password)
    else:
        raise ValueError


if __name__ == "__main__":
    main()
