from datetime import datetime


def sim_time():
    factory_start = 2700  # 9am
    diner_start = 3300  # 11 am
    nightclub_start = 6000  # 8pm

    date = datetime.utcnow()
    if date.hour % 2:
        suffix, cycle = 'PM', 3600
    else:
        suffix, cycle = 'AM', 0
    cycle += date.minute * 60 + date.second
    tshours = cycle//300 % 12
    tsmin = cycle % 300 // 5

    times = {
        'factory': job_time(cycle, factory_start),
        'diner': job_time(cycle, diner_start),
        'nightclub': job_time(cycle, nightclub_start)
    }

    return tshours, tsmin, suffix, times


def job_time(cycle, start_time):
    start_time = start_time + 7200 if cycle > start_time else start_time
    job_sec = start_time - cycle
    job_min = job_sec//60
    job_hr = job_min//60

    return job_hr, job_min % 60


def main():
    time = sim_time()
    print(time)


if __name__ == "__main__":
    main()
